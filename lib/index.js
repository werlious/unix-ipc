/*
**
**  Example of Interprocess communication in Node.js through a UNIX domain socket
**
**  Usage:
**   server>  MODE=server node ipc.example.js
**   client>  MODE=client node ipc.example.js
**
*/

var net = require('net'),
    fs = require('fs'),
    time = require('perf_hooks').performance.now

const pid = process.pid
class IPC_Server {
    on(event,handler) {
        this.listeners[event] = handler
    }
    async emit(event,data) {
        for(const ts in this.connections) {
            let connection = this.connections[ts]
            if(connection.____busy) {
                return setImmediate(this.emit, event, data);
            }
            if(!connection.write(Buffer.from(JSON.stringify({event,data})))) {
                connection.____busy = true
                connection.on('drain',() => {
                    connection.____busy = false
                })
            }
        }
    }
    ___sem(input) {
        if(this.listeners[input.event]) this.listeners[input.event](input.data)
    }
    async close() {
        await this.emit('server_close',{})
        cleanup()
        this.listeners = {}
        this.server = undefined
        this.connections = {}
        this.on = () => {}
        this.emit = () => {}
        this.___sem = () => {}
    }
    cleanup(){
        if(!this.SHUTDOWN){
            this.SHUTDOWN = true;
            console.log('\n',"Terminating.",'\n');
            if(Object.keys(this.connections).length){
                let clients = Object.keys(this.connections);
                while(clients.length){
                    let client = clients.pop();
                    this.connections[client].end(); 
                    this.connections[client].destroy()
                }
            }
            this.server.close();
        }
    }
    static connect(file) {
        return new Promise(function (resolve,reject) {
            fs.stat(file, function (err, stats) {
                if (err) {
                    resolve(new IPC_Server(file))
                    return
                }
                fs.unlink(file, function(err){
                    if(err){
                        reject(err)
                    }
                    resolve(new IPC_Server(file))
                    return
                });  
            })
        })
    }
    constructor(socket) {
        this.connections = {}
        this.listeners = {}
        const self = this
        this.server = server = net.createServer().listen(socket)
        .on('connection', (s) => {
            const ts = time()
            self.connections[ts] = socket
            s.on('close',() => {
                self.connections[ts] = undefined
                delete self.connections[ts]
            })
            s.on('data', msg => {
                self.___sem(JSON.parse(msg.toString()))
            })
            s.setTimeout(10000);
            s.on('timeout', () => {
                self.connections[ts].destroy()
                self.connections[ts] = undefined
                delete self.connections[ts]
                s.end();
            });
        })
    }
}

class IPC {
    static connect(file) {
        return new Promise((resolve,reject) => {
            resolve(new IPC(file))
        })
    }
    on(event,handler) {
        this.listeners[event] = handler
    }
    async emit(event,data) {
        if(!this.ready) return setImmediate(this.emit, event, data);
        let connection = this.client
        if(connection.____busy) {
            return setImmediate(this.emit, event, data);
        }
        if(!connection.write(Buffer.from(JSON.stringify({event,data})))) {
            connection.____busy = true
            connection.on('drain',() => {
                connection.____busy = false
            })
        }
    }
    close() {
        if(this.client) this.client.destroy()
        this.client = undefined
        delete this.client
        this.listeners = []
        this.client
    }
    constructor(socket) {
        this.listeners = []
        const self = this
        this.client = net.createConnection(socket)    
        .on('connect',(s) => {
            this.ready = true
            self.listeners['connect']()
        })
        .on('data',(msg) => {
            msg = JSON.parse(msg.toString())
            if(self.listeners[msg.event]) self.listeners[msg.event] (msg.data)
        })
    }
}


module.exports = {
    server: IPC_Server,
    client: IPC
}